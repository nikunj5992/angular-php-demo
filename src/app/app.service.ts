import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Book } from './models/book';
@Injectable({
  providedIn: 'root'
})
export class AppService {
  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) {

  }

  getProducts(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.apiUrl);
  }

  postProduct(payload: Book): Observable<Book> {
    return this.httpClient.post<Book>(this.apiUrl, payload);
  }
}
