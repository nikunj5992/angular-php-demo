import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppService } from './app.service';
import { Book } from './models/book';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  tempForm = this.formBuilder.group({
    title: ['', [Validators.required]],
    author: ['', [Validators.required]]
  });
  title = 'php-demo';
  products: Book[] = [];
  displayColumns = ['title', 'author'];
  dataSource = new MatTableDataSource<Book>();
  apiInProgress = false;
  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private _snackBar: MatSnackBar
    ) {}
  
  ngOnInit(): void {
    this.displayProducts();
  }

  submit(): void {
    if (this.tempForm.valid) {
      const payload: Book = {
        book_title: this.tempForm.get('title')?.value,
        book_author: this.tempForm.get('author')?.value        
      }
      this.apiInProgress = true;
      this.appService.postProduct(payload).subscribe((response: any) => {
        if (response) {
          this.apiInProgress = false;
          this.displayProducts();
          this._snackBar.open('Book added successfully... ', undefined, {
            duration: 2000
          });
        }
      });
    } else {
      this.tempForm.markAllAsTouched();
    }
  }

  displayProducts(): void {
    this.apiInProgress = true;
    this.appService.getProducts().subscribe((products: Book[]) => {
      this.apiInProgress = false;
      this.products = products;
      this.dataSource.data = products;
    });
  } 
}
