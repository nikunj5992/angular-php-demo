export interface Book {
    id?: number;
    book_author: string;
    book_title: string;
}